class Comment < ActiveRecord::Base
	# Module 3 Lecture 4: The Blog App - Iteration 2 (Associations)
	belongs_to :post
	# Module 3 Lecture 5: The Blog App - Iteration 3 (Validations)
	validates_presence_of :post_id
	validates_presence_of :body
end
