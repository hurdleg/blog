class Post < ActiveRecord::Base
	# Module 3 Lecture 4: The Blog App - Iteration 2 (Associations)
	has_many :comments, dependent: :destroy
	# Module 3 Lecture 5: The Blog App - Iteration 3 (Validations)
	validates_presence_of :title
	validates_presence_of :body
end
